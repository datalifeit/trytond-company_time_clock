# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.method.selection.append(
            ('company.employee.time_clock|cron_set_codes',
                'Set time clocking I/O Codes'))
        cls.method.selection.append(
            ('company.employee.time_clock|cron_confirm',
                'Confirm time clocking'))
