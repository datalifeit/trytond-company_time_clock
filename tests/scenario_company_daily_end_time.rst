==========================================
Company Time Clock Daily End Time Scenario
==========================================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from datetime import datetime, time


Install modules::

    >>> config = activate_modules(['company_time_clock', 'production_daily_end_time'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()

Employee::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Pam Beesly')
    >>> party.save()
    >>> Employee = Model.get('company.employee')
    >>> employee = Employee(party=party, company=company)
    >>> employee.save()
    >>> employee.clock_number
    '1'

Production Configuration::

    >>> Configuration = Model.get('production.configuration')
    >>> configuration = Configuration(1)
    >>> configuration.daily_end_time = time(3)
    >>> configuration.save()

Time Clock::

    >>> TimeClock = Model.get('company.employee.time_clock')
    >>> timeclock = TimeClock(clock_number='1')
    >>> timeclock.time_ = datetime(2020, 5, 18, 19)
    >>> timeclock.save()

    >>> timeclock2 = TimeClock(clock_number='1')
    >>> timeclock2.time_ = datetime(2020, 5, 19, 2, 30)
    >>> timeclock2.save()
    >>> TimeClock.set_time_clock_codes([timeclock, timeclock2], config.context)
    >>> timeclock.code
    'IN'
    >>> timeclock2.code
    'OUT'
    >>> timeclock2.date == timeclock.date == datetime(2020, 5, 18).date()
    True