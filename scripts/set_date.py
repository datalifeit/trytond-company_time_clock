#!/usr/bin/env python
import sys
from trytond.transaction import Transaction
from trytond.config import config as CONFIG
from trytond.pool import Pool

dbname = sys.argv[1]
config_file = sys.argv[2]

CONFIG.update_etc(config_file)

Pool.start()
pool = Pool(dbname)
pool.init()

context = {} # should define company on context

with Transaction().start(dbname, 0, context=context):
    TimeClock = pool.get('company.employee.time_clock')

    records = TimeClock.search([
        ('date', '=', None)])
    for record in records:
        record.date = record.on_change_with_date()
    TimeClock.save(records)

    Transaction().commit()
